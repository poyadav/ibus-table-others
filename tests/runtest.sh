#!/bin/bash
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "pushd $tmp"
        rlAssertRpm "ibus"
        rlAssertRpm "ibus-table"
        rlAssertRpm "ibus-table-code"
        rlAssertRpm "ibus-table-cyrillic"
        rlAssertRpm "ibus-table-latin"
        rlAssertRpm "ibus-table-translit"
        rlAssertRpm "ibus-table-tv"
        rlAssertRpm "ibus-table-mathwriter"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "ibus-daemon -v -r -d"
        rlRun "sleep 5" 0 "Give ibus-daemon some time to start properly."
        for name in \
            cns11643 \
            emoji-table \
            latex \
            rusle \
            rustrad \
            yawerty \
            compose \
            hu-old-hungarian-rovas \
            ipa-x-sampa \
            translit-ua \
            translit \
            telex \
            thai \
            viqr \
            vni \
            mathwriter-ibus
        do
            rlRun "/usr/libexec/ibus-engine-table --xml 2>/dev/null | grep '<name>table:${name}</name>'" \
            0 "checking whether 'ibus-engine-table --xml' can list table:${name}:"

            rlRun "ibus list-engine --name-only | grep 'table:${name}$'" \
                0 "checking whether ibus can list table:${name}:"
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
